<?php
// Ceci est un commentaire PHP sur une ligne
/* Ceci est le 2ème type de commentaire PHP
sur plusieurs lignes */

// On met la chaine de caractères "hello" dans la variable 'texte'
// Les noms de variable commencent par $ en PHP
// $texte = "squid game !  \n";

// On écrit le contenu de la variable 'texte' dans la page Web
// echo $texte;

// $prenom = "Marc";

// echo "Bonjour\n " . $prenom;
// echo "Bonjour\n $prenom";
// echo 'Bonjour\n $prenom';

// echo $prenom;
// echo "$prenom";

// $marque = 'Squid';
// $couleur = 'Rouge';
// $immatriculation = "69696969";

// echo "<p> ModelVoiture $immatriculation de marque $marque (couleur $couleur) </p>";

$voiture1 = [
    'marque' => 'Squid',
    'couleur' => 'Rouge',
    'immatriculation' => '69696969'
];
$voiture2 = [
    'marque' => 'Baka',
    'couleur' => 'Rose',
    'immatriculation' => 'BAKAMOBIL'
];
$voiture3 = [
    'marque' => 'Rayan',
    'couleur' => 'Marron',
    'immatriculation' => 'YesD4ddY'
];
$voiture4 = [
    'marque' => 'Oui',
    'couleur' => 'bleu',
    'immatriculation' => 'OuiOuiTaxi'
];

//echo "<p> ModelVoiture {$voiture['immatriculation']} de marque {$voiture['marque']} (couleur {$voiture['couleur']}) ";


$voitures = [
    0 => $voiture1,
    1 => $voiture2,
    2 => $voiture3,
    3 => $voiture4
];

echo " <h1> Liste de voitures : <h1/> \n <ul> " ;

if( empty($voitures)) {
    echo "tableau vide";
} else {
    for($i = 0; $i < count($voitures); $i++){
        echo " <li> ModelVoiture : {$voitures[$i]['immatriculation']} , marque : {$voitures[$i]['marque']} , couleur : {$voitures[$i]['couleur']} <li/>";
    }
    echo "<ul/>";
}
