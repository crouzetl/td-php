<?php
class Trajet {

    private int $id;
    private string $depart;
    private string $arrivee;
    private string $date;
    private int $nbPlaces;
    private int $prix;
    private string $conducteur_login;

    /**
     * @param int $id
     * @param string $depart
     * @param string $arrivee
     * @param string $date
     * @param int $nbPlaces
     * @param int $prix
     * @param string $conducteur_login
     */
    public function __construct(int $id, string $depart, string $arrivee, string $date, int $nbPlaces, int $prix, string $conducteur_login)
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteur_login = $conducteur_login;
    }


}