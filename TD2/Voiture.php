<?php
require_once "Model.php";
class Voiture {
   
    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises
   
    // un getter      
    public function getMarque() : string {
        return $this->marque;
    }
   
    // un setter 
    public function setMarque(string $marque) : void {
        $this->marque = $marque;
    }

    public function getCouleur() : string {
        return $this->couleur;
    }

    public function setCouleur(string $couleur) : void {
        strlen($couleur) <= 8 ? $this->couleur = $couleur : $this->couleur = substr($couleur, 0 , 8) ;
    }

    public function getImmatriculation() : string {
        return $this->immatriculation;
    }

    public function setImmatriculation(string $immatriculation) : void {
        $this->immatriculation = $immatriculation;
    }

    public function getNbSieges(): int {
        return $this->nbSieges;
    }

    public function setNbSieges(int $nbSieges): void {
        $this->nbSieges = $nbSieges;
    }

    // un constructeur
    public function __construct(
        string $marque, 
        string $couleur, 
        string $immatriculation,
        int $nbSieges
   ) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = $immatriculation;
        $this->nbSieges = $nbSieges;
    }
              
    // une methode d'affichage.
    public function afficher(){
      echo " <p> La voiture courante est {$this->getImmatriculation()}, de marque {$this->getMarque()}, de couleur {$this->getCouleur()} et possède {$this->getNbSieges()} places <p/> ";
    }

    public function construire(array $voitureFormatTableau) : ModelVoiture {
        return new static($voitureFormatTableau['immatriculation'], $voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['nbSieges']);

    }

    public static function getVoitures() : array {
        $pdoStatement = Model::getPdo()->query("SELECT * FROM voiture");
        $tab = [];

        foreach ($pdoStatement as $voitureFormatTableau) {
           $tab[] = $voitureFormatTableau;
        }
        return $tab;
    }



}