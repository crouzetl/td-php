<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;


    public function __construct(string $login, string $nom, string $prenom) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }


    public static function getUtilisateurs() : array {
        $pdoStatement = Model::getPdo()->query("SELECT * FROM utilisateurs");
        $tab = [];

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $tab[] = $utilisateurFormatTableau;
        }
        return $tab;
    }

}
