<?php
// chargement du modèle


namespace App\Covoiturage\Controller;
use App\Covoiturage\Model\ModelVoiture;

class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $voitures = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD
        self::afficheVue('view.php', ["voitures" => $voitures, "pagetitle" => "Liste des voitures", "cheminVueBody" => "voiture/list.php"]);  //"redirige" vers la vue
    }

    public static function read() : void {
            $voiture = ModelVoiture::getVoitureParImmat($_GET['immat']);
            if (empty($voiture)) {
                self::afficheVue( 'view.php', ["cheminVueBody" => 'voiture/error.php', "pagetitle" => "Error" ]);
            } else {
                self::afficheVue('view.php', ["voiture" => $voiture, "pagetitle" => "Détail", "cheminVueBody" => "voiture/detail.php" ]);
            }
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../src/View/$cheminVue"; // Charge la vue
    }

    public static function create() : void{
        self::afficheVue('view.php', ["cheminVueBody" => 'voiture/create.php', "pagetitle" => "Créer une Voiture"]);
    }

    public static function created(){
        $voiture = new ModelVoiture($_POST['marque'], $_POST['couleur'], $_POST['immatriculation'], $_POST['nbSieges']);
        $voiture->sauvegarder();
        self::afficheVue('view.php', ["cheminVueBody" => 'voiture/created.php', "pagetitle" => "Voiture créée !", "voiture" => $voiture, "voitures" =>  ModelVoiture::getVoitures()]);
    }
}
?>