<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
       <link rel="stylesheet" href="../web/css/style.css"/>
      <title><?php echo $pagetitle; ?></title>
    </head>
<body>
<header>
    <nav>
        <div> <a href="frontController.php?action=readAll"> Liste des voitures </a> </div>
        <div> <a href="frontController.php?action=readAll&controller=utilisateur"> Liste des utilisateurs </a></div>
        <div> <a href="frontController.php?action=readAll&controller=trajet"> Liste des trajets </a></div>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Caca-Man
    </p>
</footer>
</body>
</html>