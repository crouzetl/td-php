<?php
require_once ('../model/ModelVoiture.php'); // chargement du modèle
class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $voitures = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD
        frontController::afficheVue('../View/voiture/list.php', ["voitures" => $voitures]);  //"redirige" vers la vue
    }

    public static function read() : void {
            $voiture = ModelVoiture::getVoitureParImmat($_GET['immat']);
            if (empty($voiture)) {
                frontController::afficheVue('../View/voiture/error.php');
            } else {
                frontController::afficheVue('../View/voiture/detail.php', ["voiture" => $voiture]);
            }
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../view/$cheminVue"; // Charge la vue
    }

    public static function create() : void{
        frontController::afficheVue('../View/voiture/create.php');
    }

    public static function created(){
        $voiture = new ModelVoiture($_POST['marque'], $_POST['couleur'], $_POST['immatriculation'], $_POST['nbSieges']);
        $voiture->sauvegarder();
        self::readAll();
    }
}
?>