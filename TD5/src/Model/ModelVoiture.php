<?php

namespace App\Covoiturage\Model;




class ModelVoiture {
   
    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises
   
    // un getter      
    public function getMarque() : string {
        return $this->marque;
    }
   
    // un setter 
    public function setMarque(string $marque) : void {
        $this->marque = $marque;
    }

    public function getCouleur() : string {
        return $this->couleur;
    }

    public function setCouleur(string $couleur) : void {
        strlen($couleur) <= 8 ? $this->couleur = $couleur : $this->couleur = substr($couleur, 0 , 8) ;
    }

    public function getImmatriculation() : string {
        return $this->immatriculation;
    }

    public function setImmatriculation(string $immatriculation) : void {
        $this->immatriculation = $immatriculation;
    }

    public function getNbSieges(): int {
        return $this->nbSieges;
    }

    public function setNbSieges(int $nbSieges): void {
        $this->nbSieges = $nbSieges;
    }

    // un constructeur
    public function __construct(
        string $marque, 
        string $couleur, 
        string $immatriculation,
        int $nbSieges
   ) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = $immatriculation;
        $this->nbSieges = $nbSieges;
    }
              
    /* une methode d'affichage.
    public function afficher(){
      echo " <p> La voiture courante est {$this->getImmatriculation()}, de marque {$this->getMarque()}, de couleur {$this->getCouleur()} et possède {$this->getNbSieges()} places <p/> ";
    }
    */

    public static function construire(array $voitureFormatTableau) : ModelVoiture {
        return new static($voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges']);

    }

    public static function getVoitures() : array {
        $pdoStatement = Model::getPdo()->query("SELECT * FROM voiture");
        $tab = [];

        foreach ($pdoStatement as $voitureFormatTableau) {
           $tab[] = ModelVoiture::construire($voitureFormatTableau);
        }
        return $tab;
    }

    public static function getVoitureParImmat(string $immatriculation) : ?ModelVoiture {
        $sql = "SELECT * from voiture WHERE immatriculation=:immatriculationTag";
        // Préparation de la requête
        $pdoStatement = Model::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);


        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();
        if(!$voiture) {
            echo " <p> pas de voiture d'immatriculation $immatriculation </p>";
            return null;
        }

        return static::construire($voiture);
    }


    public function sauvegarder() : void {
        $sql = "INSERT INTO voiture (immatriculation, marque, couleur, nbSieges) VALUES ( :immatriculationTag , :marqueTag, :couleurTag, :nbSiegesTag )";

        $pdoStatement = Model::getPdo()->prepare($sql);

        $values = [
            "immatriculationTag" => $this->getImmatriculation(),
            "marqueTag" => $this->getMarque(),
            "couleurTag" => $this->getCouleur(),
            "nbSiegesTag" => $this->getNbSieges()
        ];

        $pdoStatement->execute($values);

    }


}